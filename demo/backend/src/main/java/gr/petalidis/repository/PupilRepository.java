package gr.petalidis.repository;

import gr.petalidis.domain.Pupil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

/**
 * Created by npetalid on 02/12/2018.
 */

public interface PupilRepository extends CrudRepository<Pupil, UUID> {

    List<Pupil> findByName(String name);

    long count();

    Page<Pupil> findAll(Pageable pageable);
}
