package gr.petalidis.dto;

import gr.petalidis.domain.Pupil;

/**
 * Created by npetalid on 02/12/2018.
 */
public class PhotoOfDayDto {

    private String caption = "";

    private byte[] photo = new byte[0];

    public PhotoOfDayDto() {
    }

    public PhotoOfDayDto(String caption, byte[] photo) {
        this.caption = caption;
        this.photo = photo;
    }

    public PhotoOfDayDto(Pupil pupil) {
        //Try 1: this.caption = pupil.getName() + ", " + "ετών " + pupil.getAge();
        if (!pupil.getName().isEmpty()) {
            this.caption = pupil.getName() + ", " + "ετών " + pupil.getAge();
        }
        this.photo = pupil.getImage();
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
}
