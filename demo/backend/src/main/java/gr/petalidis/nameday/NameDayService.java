package gr.petalidis.nameday;

import gr.petalidis.exceptions.ExternalServiceNotResponsiveException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by npetalid on 02/12/2018.
 */

@Service
public class NameDayService {

    @Autowired
    NameDayClient nameDayClient;


    public NameDayResult getNamesForToday() {

        try {
            String names = nameDayClient.getNamesForToday();

            String[] nameArray = names.split(",");
            //Try 1: return Arrays.asList(nameArray);
            //Try 2: return Arrays.asList(nameArray).stream().map(x->x.trim()).collect(Collectors.toList());
            return new NameDayResult(Arrays.asList(nameArray).stream().map(x -> x.trim()).filter(x -> !x.isEmpty()).collect(Collectors.toList()));

        } catch (ExternalServiceNotResponsiveException e) {
            //Log error
            return new NameDayResult();
        }
    }
}
