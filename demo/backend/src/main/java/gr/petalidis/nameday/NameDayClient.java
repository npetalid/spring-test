package gr.petalidis.nameday;

import gr.petalidis.exceptions.ExternalServiceNotResponsiveException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

/**
 * Created by npetalid on 02/12/2018.
 */
@Service
public class NameDayClient {
    @Value("${nameday.endpoint}")
    String endpoint;

    public String getNamesForToday() throws ExternalServiceNotResponsiveException {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        try {
            ResponseEntity<String> applicationEntity = restTemplate.exchange(endpoint,
                    HttpMethod.GET,
                    entity, String.class);
            if (applicationEntity.getStatusCode() != HttpStatus.OK) {
                throw new ExternalServiceNotResponsiveException("Η υπηρεσία Εορτολόγιο δεν είναι διαθέσιμη." + applicationEntity.getBody());
            }
            return applicationEntity.getBody();
        } catch (RestClientException e) {
            throw new ExternalServiceNotResponsiveException("Η υπηρεσία Εορτολόγιο αντιμετοπίζει κάποιο άγνωστο πρόβλημα." + e.getLocalizedMessage());
        }
    }
}
