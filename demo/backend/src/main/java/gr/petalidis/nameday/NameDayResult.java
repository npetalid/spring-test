package gr.petalidis.nameday;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by npetalid on 02/12/2018.
 */
public class NameDayResult {

    private List<String> names = new ArrayList<>();

    public NameDayResult() {
    }

    public NameDayResult(List<String> names) {
        //try 1: names.addAll(names);
        this.names.addAll(names);
    }

    public boolean isEmpty() {
        return names.isEmpty();
    }

    public String getRandomName() {
        if (names.isEmpty()) {
            return "";
        }

        long numOfEntries = names.size();

        int idx = (int) (Math.random() * (numOfEntries) - 1);

        //Try:2 int idx = (int)(Math.random() * (numOfEntries));
        //      return names.get(idx-1);
        return names.get(idx);
    }

    public List<String> getNames() {
        return new ArrayList<>(names);
    }

}
