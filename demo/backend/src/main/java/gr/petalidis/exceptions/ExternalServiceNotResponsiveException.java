package gr.petalidis.exceptions;

/**
 * Created by npetalid on 02/12/2018.
 */
public class ExternalServiceNotResponsiveException extends Exception {
    public ExternalServiceNotResponsiveException(String s) {
        super(s);
    }
}
