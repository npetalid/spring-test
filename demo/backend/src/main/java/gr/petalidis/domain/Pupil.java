package gr.petalidis.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.time.LocalDate;
import java.util.UUID;

/**
 * Created by npetalid on 02/12/2018.
 **/

@Entity
public class Pupil {

    @Id
    private UUID id = UUID.randomUUID();

    private String name = "";

    private LocalDate dateOfBirth = LocalDate.now();

    //Try 1: nothing. Fixed with integration test
    @Lob
    @Column(name = "image", columnDefinition="BLOB")
    private byte[] image = new byte[0];

    public Pupil() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getAge() {
        return LocalDate.now().getYear() - dateOfBirth.getYear();
    }
}
