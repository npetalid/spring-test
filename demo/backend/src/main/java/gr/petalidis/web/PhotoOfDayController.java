package gr.petalidis.web;

import gr.petalidis.dto.PhotoOfDayDto;
import gr.petalidis.service.PhotoOfTheDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/PhotoOfDayController")
public class PhotoOfDayController {
    @Autowired
    PhotoOfTheDayService service;


    @RequestMapping(method = RequestMethod.GET, consumes = {APPLICATION_JSON_VALUE})
    @CrossOrigin
    public @ResponseBody
    PhotoOfDayDto getPhotoOfDay() {
        return service.getPhotoOfDay();
    }
}
