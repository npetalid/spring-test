package gr.petalidis.service;

import gr.petalidis.domain.Pupil;
import gr.petalidis.dto.PhotoOfDayDto;
import gr.petalidis.nameday.NameDayResult;
import gr.petalidis.nameday.NameDayService;
import gr.petalidis.repository.PupilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by npetalid on 02/12/2018.
 */
@Service
public class PhotoOfTheDayService {

    @Autowired
    NameDayService nameDayService;

    @Autowired
    PupilRepository pupilRepository;

    public PhotoOfDayDto getPhotoOfDay() {

        NameDayResult nameDayResult = nameDayService.getNamesForToday();
        List<Pupil> pupils = new ArrayList<>();
        if (!nameDayResult.isEmpty()) {
            String randomName = nameDayResult.getRandomName();
            pupils.addAll(pupilRepository.findByName(randomName));
        } //try 1: else {

        if (pupils.isEmpty()) {
            pupilRepository.findAll().forEach(pupils::add);
        }
        Pupil pupil = new Pupil();
        if (!pupils.isEmpty()) {
             pupil = pupils.get(getRandomEntry(pupils.size()));
        }
        return new PhotoOfDayDto(pupil);

    }

    private int getRandomEntry(int size) {
        int idx = (int) (Math.random() * size - 1);
        return idx;
    }


}
