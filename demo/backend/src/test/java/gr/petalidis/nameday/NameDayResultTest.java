package gr.petalidis.nameday;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by npetalid on 02/12/2018.
 */
public class NameDayResultTest {

    @Test
    public void testWithEmptyListShouldReturnEmptyString() {
        NameDayResult nameDayResult = new NameDayResult();

        assertEquals("", nameDayResult.getRandomName());
    }

    @Test
    public void testWithOneElementListShouldReturnTheSameElement() {
        NameDayResult nameDayResult = new NameDayResult(Arrays.asList(new String[]{"Nikos"}));

        assertEquals("Nikos", nameDayResult.getRandomName());
    }
}