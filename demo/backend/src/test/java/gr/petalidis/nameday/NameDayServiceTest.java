package gr.petalidis.nameday;

import gr.petalidis.exceptions.ExternalServiceNotResponsiveException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by npetalid on 02/12/2018.
 */
@RunWith(SpringRunner.class)
//@SpringBootTest
public class NameDayServiceTest {

    @TestConfiguration
    static class NameDayServiceImplTestContextConfiguration {

        @Bean
        public NameDayService nameDayService() {
            return new NameDayService();
        }
    }
    @Autowired
    private NameDayService service;

    @MockBean
    private NameDayClient nameDayClient;

    @Test
    public void getNamesForTodayWhenEmpty() throws Exception {
        when(nameDayClient.getNamesForToday()).thenReturn("");

        List<String> expectedResult = new ArrayList<>();

        List<String> actualResult = service.getNamesForToday().getNames();
        assertThat(actualResult, is(expectedResult));
    }


    @Test
    public void getNamesForTodayWhenOneName() throws Exception {
        when(nameDayClient.getNamesForToday()).thenReturn("Nikos");

        List<String> expectedResult = Arrays.asList(new String[]{"Nikos"});

        List<String> actualResult = service.getNamesForToday().getNames();
        assertThat(actualResult, is(expectedResult));


    }

    @Test
    public void getNamesForTodayWhenTwoNames() throws Exception {
        when(nameDayClient.getNamesForToday()).thenReturn("Νίκος, Νικολέττα");

        List<String> expectedResult = Arrays.asList(new String[]{"Νίκος", "Νικολέττα"});

        List<String> actualResult = service.getNamesForToday().getNames();
        assertThat(actualResult, is(expectedResult));


    }

    @Test
    public void getNamesForTodayWhenException() throws Exception {
        when(nameDayClient.getNamesForToday()).thenThrow(new ExternalServiceNotResponsiveException("Κάποιο λάθος"));

        List<String> expectedResult = new ArrayList<>();

        List<String> actualResult = service.getNamesForToday().getNames();
        assertThat(actualResult, is(expectedResult));
    }

}