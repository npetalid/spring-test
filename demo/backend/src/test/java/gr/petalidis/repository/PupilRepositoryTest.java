package gr.petalidis.repository;

import gr.petalidis.domain.Pupil;
import org.apache.tomcat.jni.Local;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.OffsetTime;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PupilRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PupilRepository pupilRepository;

    private static byte[] image = new byte[0];

    @BeforeClass
    public static void  setUpClass() throws Exception
    {
        ClassLoader classLoader = PupilRepositoryTest.class.getClassLoader();
        File file = new File(classLoader.getResource("sample.jpg").getFile());
        image = new byte[(int) file.length()];
        try (InputStream is = new FileInputStream(file)) {
            is.read(image);
        }
    }
    @Test
    public void findByName() {
       Pupil pupil = new Pupil();
       pupil.setName("Κώστας");
       pupil.setDateOfBirth(LocalDate.of(2011,1,1));
       pupil.setImage(image);

       entityManager.persist(pupil);
       entityManager.flush();

       List<Pupil> results = pupilRepository.findByName("Κώστας");
       assertEquals(1,results.size());

       Pupil result = results.get(0);

       assertThat(result.getDateOfBirth(),is(LocalDate.of(2011,1,1)));
       assertArrayEquals(image,result.getImage());
    }
}