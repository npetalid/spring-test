package gr.petalidis.service;

import gr.petalidis.domain.Pupil;
import gr.petalidis.dto.PhotoOfDayDto;
import gr.petalidis.nameday.NameDayResult;
import gr.petalidis.nameday.NameDayService;
import gr.petalidis.repository.PupilRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by npetalid on 02/12/2018.
 */
@RunWith(SpringRunner.class)
//@SpringBootTest
public class PhotoOfTheDayServiceTest {

    @TestConfiguration
    static class PhotoOfTheDayServiceImplTestContextConfiguration {

        @Bean
        public PhotoOfTheDayService photoOfTheDayService() {
            return new PhotoOfTheDayService();
        }
    }
    @Autowired
    private PhotoOfTheDayService service;

    @MockBean
    private NameDayService nameDayService;

    @MockBean
    private PupilRepository pupilRepository;

    @Test
    public void getPhotoOfDayWhenNoNameDayAndNoPupilsExist() throws Exception {
        when(nameDayService.getNamesForToday()).thenReturn(new NameDayResult());
        when(pupilRepository.findAll()).thenReturn(new ArrayList<>());

        PhotoOfDayDto photoOfDayDto = service.getPhotoOfDay();

        assertEquals("", photoOfDayDto.getCaption());
        assertArrayEquals(new byte[0], photoOfDayDto.getPhoto());

    }

    @Test
    public void getPhotoOfDayWhenNameDayExistsAndNoPupilsExist() throws Exception {
        when(nameDayService.getNamesForToday()).thenReturn(
                new NameDayResult(Arrays.asList(new String[]{"Nikos"})));
        when(pupilRepository.findAll()).thenReturn(new ArrayList<>());

        PhotoOfDayDto photoOfDayDto = service.getPhotoOfDay();

        assertEquals("", photoOfDayDto.getCaption());
        assertArrayEquals(new byte[0], photoOfDayDto.getPhoto());

    }

    @Test
    public void getPhotoOfDayWhenNoNameDayAndPupilsExist() throws Exception {
        Pupil pupil = new Pupil();
        pupil.setName("Γιάννης");
        pupil.setDateOfBirth(LocalDate.now());

        List<Pupil> pupils = new ArrayList<>();
        pupils.add(pupil);
        when(nameDayService.getNamesForToday()).thenReturn(new NameDayResult());
        when(pupilRepository.findAll()).thenReturn(pupils);

        PhotoOfDayDto photoOfDayDto = service.getPhotoOfDay();

        assertEquals("Γιάννης, ετών 0", photoOfDayDto.getCaption());
        assertArrayEquals(new byte[0], photoOfDayDto.getPhoto());

    }
    @Test
    public void getPhotoOfDayWhenNameDayAndPupilsExist() throws Exception {
        Pupil pupil = new Pupil();
        pupil.setName("Γιάννης");
        pupil.setDateOfBirth(LocalDate.now());

        Pupil pupil2 = new Pupil();
        pupil2.setName("Νίκος");
        pupil2.setDateOfBirth(LocalDate.now());

        List<Pupil> allPupils = new ArrayList<>();
        allPupils.add(pupil);
        allPupils.add(pupil2);

        List<Pupil> onePupil = new ArrayList<>();
        onePupil.add(pupil2);

        when(nameDayService.getNamesForToday()).thenReturn(
                new NameDayResult(Arrays.asList(new String[]{"Νίκος"})));
        when(pupilRepository.findByName("Νίκος")).thenReturn(onePupil);

        when(pupilRepository.findAll()).thenReturn(allPupils);

        PhotoOfDayDto photoOfDayDto = service.getPhotoOfDay();

        assertEquals("Νίκος, ετών 0", photoOfDayDto.getCaption());
        assertArrayEquals(new byte[0], photoOfDayDto.getPhoto());

    }

    @Test
    public void getPhotoOfDayWhenNameDayAndPupilsIsNotInBaseExist() throws Exception {
        Pupil pupil = new Pupil();
        pupil.setName("Γιάννης");
        pupil.setDateOfBirth(LocalDate.now());

        List<Pupil> allPupils = new ArrayList<>();
        allPupils.add(pupil);

        when(nameDayService.getNamesForToday()).thenReturn(
                new NameDayResult(Arrays.asList(new String[]{"Κώστας"})));
        when(pupilRepository.findByName("Κώστας")).thenReturn(new ArrayList<Pupil>());
        when(pupilRepository.findAll()).thenReturn(allPupils);

        PhotoOfDayDto photoOfDayDto = service.getPhotoOfDay();

        assertEquals("Γιάννης, ετών 0", photoOfDayDto.getCaption());
        assertArrayEquals(new byte[0], photoOfDayDto.getPhoto());

    }

}