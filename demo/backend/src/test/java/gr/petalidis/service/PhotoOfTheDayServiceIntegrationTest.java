package gr.petalidis.service;

import gr.petalidis.IntegrationTest;
import gr.petalidis.domain.Pupil;
import gr.petalidis.dto.PhotoOfDayDto;
import gr.petalidis.nameday.NameDayResult;
import gr.petalidis.nameday.NameDayService;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.UUID;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by npetalid on 02/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
//Normally this would not be needed. See comment in .sql file
@Transactional
@Category(IntegrationTest.class)
public class PhotoOfTheDayServiceIntegrationTest {

    private static byte[] image = new byte[0];

    //Normally this would not be needed. See comment in .sql file
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private PhotoOfTheDayService service;

    @MockBean
    private NameDayService nameDayService;

    @Before
    public void setUpClass() throws Exception {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("sample.jpg").getFile());
        image = new byte[(int) file.length()];
        try (InputStream is = new FileInputStream(file)) {
            is.read(image);
        }
        //Normally this would go into the .sql file, but h2 is not loading
        // binary files. See comment in .sql file

        UUID id = UUID.fromString("9304bb27-ba20-11e7-8aba-21b1586d3815");
        Pupil pupil = entityManager.find(Pupil.class,
                id);
        pupil.setImage(image);
        entityManager.persist(pupil);
        UUID id2 = UUID.fromString("9304bb27-ba20-11e7-8aba-21b1586d4815");

        pupil = entityManager.find(Pupil.class,id2);
        pupil.setImage(image);
        entityManager.persist(pupil);
        entityManager.flush();

    }

    @Test
    public void getPhotoOfDayWhenNameDayAndPupilsExist() throws Exception {
        when(nameDayService.getNamesForToday()).thenReturn(
                new NameDayResult(Arrays.asList(new String[]{"Νίκος"})));

        PhotoOfDayDto photoOfDayDto = service.getPhotoOfDay();

        assertEquals("Νίκος, ετών 7", photoOfDayDto.getCaption());
        assertArrayEquals(image, photoOfDayDto.getPhoto());

    }

}