package gr.petalidis.web;

import gr.petalidis.IntegrationTest;
import gr.petalidis.domain.Pupil;
import gr.petalidis.nameday.NameDayResult;
import gr.petalidis.nameday.NameDayService;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.UUID;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.is;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
@Transactional
@Category(IntegrationTest.class)
public class PhotoOfDayControllerIntegrationTest {
    private static byte[] image = new byte[0];
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private NameDayService nameDayService;

    @Before
    public void setUpClass() throws Exception {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("sample.jpg").getFile());
        image = new byte[(int) file.length()];
        try (InputStream is = new FileInputStream(file)) {
            is.read(image);
        }
        //Normally this would go into the .sql file, but h2 is not loading
        // binary files. See comment in .sql file

        UUID id = UUID.fromString("9304bb27-ba20-11e7-8aba-21b1586d3815");
        Pupil pupil = entityManager.find(Pupil.class,
                id);
        pupil.setImage(image);
        entityManager.persist(pupil);
        UUID id2 = UUID.fromString("9304bb27-ba20-11e7-8aba-21b1586d4815");

        pupil = entityManager.find(Pupil.class,id2);
        pupil.setImage(image);
        entityManager.persist(pupil);
        entityManager.flush();

    }
    @Test
    public void getPhotoOfDay() throws Exception {
        when(nameDayService.getNamesForToday()).thenReturn(
                new NameDayResult(Arrays.asList(new String[]{"Γιάννης"})));

         mvc.perform(get("/api/PhotoOfDayController")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                 .andExpect(jsonPath("$.caption", is("Γιάννης, ετών 7")))
                 //Only test the beginning of the uu-encoded binary file
                .andExpect(jsonPath("$.photo", startsWith("/9j/4AA")));
    }
}