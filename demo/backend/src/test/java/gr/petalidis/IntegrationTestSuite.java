package gr.petalidis;

import gr.petalidis.service.PhotoOfTheDayServiceIntegrationTest;
import gr.petalidis.web.PhotoOfDayControllerIntegrationTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory(IntegrationTest.class)

@Suite.SuiteClasses({
        PhotoOfTheDayServiceIntegrationTest.class,
        PhotoOfDayControllerIntegrationTest.class
})
public class IntegrationTestSuite {
}
