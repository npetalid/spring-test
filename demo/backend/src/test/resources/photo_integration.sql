-- h2 seems to have a bug, not loading binary data from classpath: See https://github.com/h2database/h2database/issues/446
INSERT INTO PUPIL(id,name,date_of_birth) values ( E'9304bb27ba2011e78aba21b1586d3815','Νίκος','2011-01-01');
INSERT INTO PUPIL(id,name,date_of_birth) values ( E'9304bb27ba2011e78aba21b1586d4815','Γιάννης','2011-01-01');
