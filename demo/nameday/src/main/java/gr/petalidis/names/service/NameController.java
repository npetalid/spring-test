package gr.petalidis.names.service;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by npetalid on 02/12/2018.
 */

@RestController
@RequestMapping("/currentDay/")
public class NameController {

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @CrossOrigin
    public
    @ResponseBody
    String getNames() throws IOException {

        return "Νίκος, Νικόλαος, Νίκη, Νικολλέτα";

    }
}
