/**
 * Created by npetalid on 02/12/2018.
 */
package gr.petalidis.names;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"gr.petalidis.names.service"})

public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}


